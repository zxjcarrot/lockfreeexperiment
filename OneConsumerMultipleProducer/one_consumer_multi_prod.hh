/*
 * Copyright (c) 2013, Marwan Burelle
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// Lock Free Queue based on Micheal&Scott PODC96 article
// A modified version with one blocking consumer but still multiple lock-free
// consumers

// TODO:
// * Comments !
// * more C++ like interface, presentation ...
// * blocked iterator ? does it makes sense ?

#include <atomic>

#include <hazard_pointer.hh>

#ifndef LOCKFREEEXPERIMENT_ONECONSUMERMULTIPLEPRODUCER_ONE_CONSUMER_MULTI_PROD_HH_
#define LOCKFREEEXPERIMENT_ONECONSUMERMULTIPLEPRODUCER_ONE_CONSUMER_MULTI_PROD_HH_

template <typename T>
class one_consumer_multi_prod {
public:

  one_consumer_multi_prod() : Head(new Node()) {
    Tail = Head.load();
  }

  inline void push(T x) noexcept;
  inline bool pop(T& rvalue) noexcept;

private:

  struct Node {
    T                   value;
    std::atomic<Node*>  next;
    Node() : next(0) {}
    Node(T x, Node* n) : value(x), next(n) {}
  };

  hazard_pointer_manager<Node,2>        mem_manager;
  std::atomic<Node*>    Head;
  std::atomic<Node*>    Tail;

};

template <typename T>
inline void one_consumer_multi_prod<T>::push(T x) noexcept {
  hprecord_guard<Node,2>        hp(mem_manager);
  Node                         *node = new Node(x,0);
  Node                         *tail, *next;
  do {
    tail = Tail.load();
    hp[0] = tail;
    if (Tail != tail) continue;
    next = tail->next.load();
    if (Tail != tail) continue;
    if (next != 0) {
      Tail.compare_exchange_weak(tail, next);
      continue;
    }
    if (tail->next.compare_exchange_weak(next,node)) break;
  } while (true);
  Tail.compare_exchange_weak(tail, node);
}

template <typename T>
inline bool one_consumer_multi_prod<T>::pop(T& rvalue) noexcept {
  hprecord_guard<Node,2>        hp(mem_manager);
  Node                 *head, *tail, *next;
  do {
    head = Head.load();
    next = head->next.load();
    tail = Tail.load();
    // don't handle the blocking here
    if (next == 0) return false;
    // this is the only case where we should try to interract with
    // the lock-free nature of the queue.
    if (head == tail) {
      Tail.compare_exchange_weak(tail, next);
      continue;
    }
    // reaching that point, we know that next nor head won't change
    rvalue = next->value;
    Head = next;
    hp.retire_node(head);
    return true;
  } while (true);
  return true;
}

#endif
