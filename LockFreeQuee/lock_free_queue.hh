/*
 * Copyright (c) 2013, Marwan Burelle
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// Lock Free Queue based on Micheal&Scott PODC96 article

// TODO:
// * Comments !
// * more C++ like interface, presentation ...
// * blocked iterator ? does it makes sense ?

#include <atomic>

#include <hazard_pointer.hh>

#ifndef LOCKFREEEXPERIMENT_LOCKFREEQUEUE_LOCK_FREE_QUEUE_HH_
#define LOCKFREEEXPERIMENT_LOCKFREEQUEUE_LOCK_FREE_QUEUE_HH_

template <typename T>
class lock_free_queue {
public:

  lock_free_queue() : Head(new Node()) {
    Tail = Head.load(std::memory_order_relaxed);
  }

  inline void push(T x) noexcept;
  inline bool pop(T& rvalue) noexcept;

private:

  struct Node {
    T                   value;
    std::atomic<Node*>  next;
    Node() : next(0) {}
    Node(T x, Node* n) : value(x), next(n) {}
  };

  hazard_pointer_manager<Node,2>        mem_manager;
  mutable std::atomic<Node*>            Head;
  mutable std::atomic<Node*>            Tail;

};

template <typename T>
inline void lock_free_queue<T>::push(T x) noexcept {
  hprecord_guard<Node,2>        hp(mem_manager);
  Node                         *node = new Node(x,0);
  Node                         *tail, *next;
  do {
    tail = Tail.load(std::memory_order_relaxed);
    hp[0] = tail;
    if (Tail != tail) continue;
    next = tail->next.load(std::memory_order_relaxed);
    if (Tail != tail) continue;
    if (next != 0) {
      Tail.compare_exchange_strong(tail, next,
                                   std::memory_order_release,
                                   std::memory_order_relaxed);
      continue;
    }
    if (tail->next.compare_exchange_strong(next,node,
                                           std::memory_order_release,
                                           std::memory_order_relaxed)) break;
  } while (true);
  Tail.compare_exchange_strong(tail, node,
                               std::memory_order_release,
                               std::memory_order_relaxed);
}

template <typename T>
inline bool lock_free_queue<T>::pop(T& rvalue) noexcept {
  hprecord_guard<Node,2>        hp(mem_manager);
  Node                         *head, *tail, *next;
  do {
    head = Head.load(std::memory_order_relaxed);
    hp[0] = head;
    if (Head != head) continue;
    tail = Tail.load(std::memory_order_relaxed);
    next = head->next.load(std::memory_order_relaxed);
    hp[1] = next;
    if (Head != head) continue;
    if (next == 0) return false;
    if (head == tail) {
      Tail.compare_exchange_strong(tail, next,
                                   std::memory_order_release,
                                   std::memory_order_relaxed);
      continue;
    }
    if (Head.compare_exchange_strong(head, next,
                                     std::memory_order_release,
                                     std::memory_order_relaxed)) break;
  } while (true);
  rvalue = next->value;
  hp.reset();
  hp.retire_node(head);
  return true;
}

#endif
