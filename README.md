LockFreeExperiment is an attempt to implement lock-free (and possibly
wait-free) data-structures using C++11 new features (mainly atomic types.)
