// Bench for my own queue

#include <cstdlib>
#include <cstdio>
#include <thread>

#include "locked_queue.hh"

#include "time_guard.hh"
#include "bencher.hh"

void initial_fill(int amount, locked_queue<int>* q) {
  for (int i = 0; i < amount; ++i)
    q->push(i);
}

struct HazardBench : public Bench<locked_queue<int>> {
  typedef locked_queue<int> Q;
  virtual void launch_worker(int id);
  virtual void pre_work();

  virtual ~HazardBench() {};

  HazardBench(int nbw, int i, Q *_q)
    : Bench(nbw,i,_q) {}

};

void pusher(const int end, locked_queue<int>* q) {
  for (int i = 0; i < end; ++i)
    q->push(i);
}

int poper(const int end, locked_queue<int>* q) {
  int                   sum = 0, i = 0;
  while (i < end) {
    int                 k;
    if (q->pop(k)) {
      sum = (sum + k) % end;
      i += 1;
    }
  }
  return sum;
}

void HazardBench::launch_worker(int id) {
  workers[id] = new std::thread([&](int th_id) {
      int               sum = 0;
      int               end = iter;
      pthread_barrier_wait(&bready);
      if (th_id % 2 == 0)
        pusher(end, q);
      else
        sum += poper(end,q) + th_id;
    },id);
}

void HazardBench::pre_work() { return; }

int main(int ac, char *av[]) {
  int           nbw = 16;
  if (ac > 1)
    nbw = atoi(av[1]);
  auto q = new locked_queue<int>();

  { time_guard          tg("initial fill, ");
    initial_fill(ITER*nbw,q);
  }

  HazardBench      b(nbw,ITER,q);
  printf("%s,",av[0]);
  b.bench();
  return 0;
}
