#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <thread>
#include <chrono>
#include <atomic>
#include <unordered_map>

#include <one_consumer_multi_prod.hh>

std::atomic<bool>       running;

struct msg {
  uint32_t      a, b;
  void print(char *pref) {
    printf("<%s> %02u:%u\n",pref,a,b);
  }
  msg() {}
  msg(uint32_t _a, uint32_t _b) : a(_a), b(_b) {}
};

static inline
uint32_t max(uint32_t a, uint32_t b)
{ return a > b ? a : b; }

struct statistic {
  std::unordered_map<uint32_t,uint32_t> ids;
  void add(msg m) {
    std::unordered_map<uint32_t,uint32_t>::iterator x = ids.find(m.a);
    x->second = max(m.b, x->second);
  }
};

void consumer(one_consumer_multi_prod<msg>& q) {
  uint32_t      maxid = 0, after = 0;
  bool          more = true;
  while (running.load(std::memory_order_relaxed) || more) {
    msg         m;
    if (q.pop(m)) {
      if (more) ++after;
      more = true;
      maxid = max(maxid, m.a);
      m.print("rec");
    }
    else {
      more = false;
      std::this_thread::yield();
    }
  }
  printf("maxid: %u\n", maxid);
  printf("after: %u\n", after);
}

void producer(one_consumer_multi_prod<msg>& q, uint32_t id) {
  uint32_t      n = 0;
  do {
    msg         m(id, n++);
    q.push(m);
  } while (running.load(std::memory_order_relaxed));
}

int main(int ac, char *av[]) {
  unsigned      workers = 8;
  if (ac > 1) workers = atoi(av[1]);
  running.store(true);
  one_consumer_multi_prod<msg>  q;
  std::thread                   cons(consumer, std::ref(q));
  std::thread                 **prods = new std::thread*[workers];
  int                           id = 1;
  for (auto cur = prods; cur != prods + workers; ++cur, ++id)
    *cur = new std::thread(producer, std::ref(q), id);

  std::this_thread::sleep_for(std::chrono::milliseconds(200));
  running.store(false, std::memory_order_relaxed);

  cons.join();
  for (auto cur = prods; cur != prods + workers; ++cur)
    (*cur)->join();

  return 0;
}
