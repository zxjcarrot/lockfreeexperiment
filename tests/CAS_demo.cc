// This is a simple demo of CAS primitive

#include <cassert>
#include <thread>
#include <atomic>
#include <iostream>

template<typename T>
T fetch_and_add(std::atomic<T>& x, T value) {
  T             tmp;
  tmp = x.load();
  while (!x.compare_exchange_weak(tmp, tmp+value)) {}
  return tmp+value;
}

class mylock {
public:

  struct guard {
    guard(mylock& _lock) : lock(_lock) {
      lock.lock();
    }
    ~guard() {
      lock.unlock();
    }
    mylock&           lock;
  };

  mylock(): _on(0) {}
  mylock(const mylock& x) = delete;
  void lock() {
    // C++11 CAS need a ref
    int                 tmp = 0;
    while (!_on.compare_exchange_weak(tmp, 1)) {
      tmp = 0; // needed since C++11 CAS back-up the changed value in tmp
      std::this_thread::yield(); // don't waiste cycles
    }
  }

  void unlock() {
    _on = 0; // ok atomic store
  }

  bool try_lock() {
    int                 tmp = 0;
    return _on.compare_exchange_weak(tmp,1);
  }

private:
  // we use here a CAS for the demo, but C++11 provide atomic_flag which should
  // be more accurate here.
  std::atomic<int>      _on;
};

void spin_count(int& x, int bound, int amount, mylock& lock) {
  for (int i = 0; i < bound; ++i) {
    std::this_thread::sleep_for(std::chrono::nanoseconds(100));
    mylock::guard       g(lock);
    x += amount;
  }
}

int main() {
  {
    std::atomic<int>    x;
    x = 0;
    std::thread         worker01
      ([&]{
        for (unsigned i = 0; i < 10000; ++i) {
          std::this_thread::sleep_for(std::chrono::nanoseconds(100));
          fetch_and_add(x, 2);
        }
      });
    std::thread         worker02
      ([&]{
        for (unsigned i = 0; i < 10000; ++i) {
          std::this_thread::sleep_for(std::chrono::nanoseconds(100));
          fetch_and_add(x, 3);
        }
      });
    worker01.join();
    worker02.join();
    assert(x == 50000);
    std::cout << "x = " << x << std::endl;
  }
  {
    int                 x = 0;
    mylock              lock{};
    std::thread         worker01(spin_count,std::ref(x),10000,2,std::ref(lock));
    std::thread         worker02(spin_count,std::ref(x),10000,3,std::ref(lock));
    worker01.join();
    worker02.join();
    assert(x == 50000);
    std::cout << "x = " << x << std::endl;
  }
  return 0;
}
