// Benching concurrent queue

#include <pthread.h>
#include <cstdio>
#include <thread>
#include <string>
#include <sstream>

#include "time_guard.hh"

#ifndef LOCKFREEEXPERIMENT_TESTS_BENCHER_HH_
#define LOCKFREEEXPERIMENT_TESTS_BENCHER_HH_

template<typename T>
static
std::string to_string(T x) {
  std::stringstream     ss;
  ss << x;
  return ss.str();
}

template<typename QueueKind>
struct Bench {
  std::thread **workers;
  int                   nb_workers, iter;
  QueueKind            *q;

  pthread_barrier_t     bready, bdone;

  virtual void launch_worker(int id) = 0;
  virtual void pre_work() = 0;

  virtual ~Bench() {}

  Bench(int nbw, int i, QueueKind *_q)
    : nb_workers(nbw), iter(i), q(_q)
  {
    workers = new std::thread*[nbw];
    pthread_barrier_init(&bready,NULL,nb_workers+1);
    pthread_barrier_init(&bdone,NULL,nb_workers+1);
  }

  void bench() {

    for (int i=0; i<nb_workers; ++i)
      this->launch_worker(i);
    this->pre_work();

    pthread_barrier_wait(&bready);

    { time_guard        tg(to_string(nb_workers)+",");
    for (auto i=0; i < nb_workers; ++i)
      workers[i]->join();
    }

  }

};

#endif
