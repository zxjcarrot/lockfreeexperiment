// Bench for my own queue

#include <cstdlib>
#include <cstdio>
#include <thread>

#include <lock_free_queue.hh>

#include "bencher.hh"

struct HazardBench : public Bench<lock_free_queue<int>> {
  typedef lock_free_queue<int> Q;
  virtual void launch_worker(int id);
  virtual void pre_work();

  virtual ~HazardBench() {};

  HazardBench(int nbw, int i, Q *_q)
    : Bench(nbw,i,_q) {}

};

double fibo(unsigned n) {
  if (n < 2) return (double)n;
  return fibo(n-1) + fibo(n-2);
}

static inline
unsigned abs_diff(unsigned a, unsigned b) {
  return a < b ? b - a : a - b;
}

void HazardBench::launch_worker(int id) {
  workers[id] = new std::thread([&](int th_id) {
      int               sum = 0;
      int               end = iter;
      double            f = 0;

      pthread_barrier_wait(&bready);

      for (int j=0; j != end; ++j) {
        int             k;
        f += fibo(1 + ((th_id + sum) % 20));
        q->push(j);
        f += fibo(1 + ((abs_diff(sum,th_id)) % 20));
        if (q->pop(k))
          sum = (sum + k) % end;
      }
      sum += th_id;
      f += th_id;
    },id);
}

void HazardBench::pre_work() { return; }

int main(int ac, char *av[]) {
  int           nbw = 16;
  if (ac > 1)
    nbw = atoi(av[1]);
  auto q = new lock_free_queue<int>();
  HazardBench      b(nbw,ITER,q);
  printf("%s,",av[0]);
  b.bench();
  return 0;
}
