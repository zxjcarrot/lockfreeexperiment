#include <cstdlib>
#include <iostream>

#include "time_guard.hh"

void f() {
  volatile int x = 0;
  for (auto i=0; i < 10000; ++i)
    x += i;
  std::cout << x << std::endl;
}

int main() {
  {time_guard   tg("time mesure: ");
    f();
  }
  return 0;
}
