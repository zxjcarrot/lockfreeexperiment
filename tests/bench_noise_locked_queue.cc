// Bench for my own queue

#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <thread>
#include <atomic>

#include "locked_queue.hh"

#include "bencher.hh"

struct noise_locked_queue : public Bench<locked_queue<int>> {
  typedef locked_queue<int> Q;
  virtual void launch_worker(int id);
  virtual void pre_work();

  virtual ~noise_locked_queue() {};

  noise_locked_queue(int nbw, int i, Q *_q)
    : Bench(nbw,i,_q) {}

};

void noise_locked_queue::launch_worker(int id) {
  workers[id] = new std::thread([&](int th_id) {
      int               sum = 0;
      int               end = iter;

      pthread_barrier_wait(&bready);

      for (int j=0; j != end; ++j) {
        int             k;
        q->push(j);
        if (q->pop(k))
          sum = (sum + k) % end;
      }
      sum += th_id;
    },id);
}

void noise_locked_queue::pre_work() { return; }

std::uint64_t ack(std::uint64_t m, std::uint64_t n) {
  return m > 0 ? ack(m-1, (n > 0 ? ack(m,n-1) : 1 )) : n + 1;
}

std::thread** occupy(int noise, std::atomic_flag& run) {
  std::thread          **wrk = new std::thread*[noise];
  for (auto cur = wrk; cur != wrk + noise; ++cur) {
    *cur = new std::thread([&run]() -> void {
        volatile std::uint64_t r = 0;
        while (run.test_and_set()) {
          r = (r + ack(3,10)) % 32;
        }
        run.clear();
      });
  }
  return wrk;
}

void wait_occupy(std::thread** wrk, int noise) {
  for (auto cur = wrk; cur != wrk + noise; ++cur)
    (*cur)->join();
}

int main(int ac, char *av[]) {
  int                   nbw = 16, noise = 16;
  std::atomic_flag      run(ATOMIC_FLAG_INIT);
  std::thread          **wrk;
  run.test_and_set();
  if (ac > 1)
    nbw = atoi(av[1]);
  if (ac > 2)
    noise = atoi(av[2]);
  auto q = new locked_queue<int>();
  noise_locked_queue       b(nbw,ITER,q);
  printf("%s,",av[0]);
  wrk = occupy(noise, run);
  b.bench();
  run.clear();
  wait_occupy(wrk, noise);
  return 0;
}
