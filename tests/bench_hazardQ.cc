// Bench for my own queue

#include <cstdlib>
#include <cstdio>
#include <thread>

#include <lock_free_queue.hh>

#include "bencher.hh"

struct HazardBench : public Bench<lock_free_queue<int>> {
  typedef lock_free_queue<int> Q;
  virtual void launch_worker(int id);
  virtual void pre_work();

  virtual ~HazardBench() {};

  HazardBench(int nbw, int i, Q *_q)
    : Bench(nbw,i,_q) {}

};

void HazardBench::launch_worker(int id) {
  workers[id] = new std::thread([&](int th_id) {
      int               sum = 0;
      int               end = iter;

      pthread_barrier_wait(&bready);

      for (int j=0; j != end; ++j) {
        int             k;
        q->push(j);
        if (q->pop(k))
          sum = (sum + k) % end;
      }
      sum += th_id;
    },id);
}

void HazardBench::pre_work() { return; }

int main(int ac, char *av[]) {
  int           nbw = 16;
  if (ac > 1)
    nbw = atoi(av[1]);
  auto q = new lock_free_queue<int>();
  HazardBench      b(nbw,ITER,q);
  printf("%s,",av[0]);
  b.bench();
  return 0;
}
