/*
 * Copyright (c) 2013, Marwan Burelle
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// Wait Free Queue based on Kogan&Petrank PPOPP12

#include <cstdint>
#include <atomic>

#include <hazard_pointer.hh>

#ifndef LOCKFREEEXPERIMENT_WAITFREEQUEUE_WAIT_FREE_QUEUE_HH_
#define LOCKFREEEXPERIMENT_WAITFREEQUEUE_WAIT_FREE_QUEUE_HH_

template <typename T, unsigned NUM_THR, unsigned HELPING_DELAY>
class wait_free_queue {
public:


  inline void push(T x) noexcept;
  inline bool pop(T& rvalue) noexcept;

private:

  struct Node {
    T                   value;
    std::atomic<Node*>  next;
    int                 enq_th_id;
    Node(T x) : value(x), next(0), enq_th_id(-1) {}
  };

  struct op_desc {
    op_desc(std::uint64_t ph, bool pe, bool en, Node no) :
      phase(ph), pending(pe), enqueue(en), node(no) {}
    std::uint64_t       phase;
    bool                pending, enqueue;
    Node                node;
  };

  struct help_record {

    help_record() : cur_th_id(-1) {
      reset();
    }

    void reset() {
      cur_th_id = (cur_th_id + 1) % NUM_THR;
      last_phase = state[cur_th_id]->phase;
      next_check = HELPING_DELAY;
    }

    int                   cur_th_id;
    std::uint64_t         last_phase, next_check;
  };


  hazard_pointer_manager<Node,2>        mem_manager;
  mutable std::atomic<Node*>            Head;
  mutable std::atomic<Node*>            Tail;
  std::vector< std::atomic<op_desc*> >  state;
  help_record                          *help_records;

};

template <typename T>
inline void wait_free_queue<T>::push(T x) noexcept {
  hprecord_guard<Node,2>        hp(mem_manager);
  Node                         *node = new Node(x,0);
  Node                         *tail, *next;
  do {
    tail = Tail.load(std::memory_order_relaxed);
    hp[0] = tail;
    if (Tail != tail) continue;
    next = tail->next.load(std::memory_order_relaxed);
    if (Tail != tail) continue;
    if (next != 0) {
      Tail.compare_exchange_strong(tail, next,
                                   std::memory_order_release,
                                   std::memory_order_relaxed);
      continue;
    }
    if (tail->next.compare_exchange_strong(next,node,
                                           std::memory_order_release,
                                           std::memory_order_relaxed)) break;
  } while (true);
  Tail.compare_exchange_strong(tail, node,
                               std::memory_order_release,
                               std::memory_order_relaxed);
}

template <typename T>
inline bool wait_free_queue<T>::pop(T& rvalue) noexcept {
  hprecord_guard<Node,2>        hp(mem_manager);
  Node                         *head, *tail, *next;
  do {
    head = Head.load(std::memory_order_relaxed);
    hp[0] = head;
    if (Head != head) continue;
    tail = Tail.load(std::memory_order_relaxed);
    next = head->next.load(std::memory_order_relaxed);
    hp[1] = next;
    if (Head != head) continue;
    if (next == 0) return false;
    if (head == tail) {
      Tail.compare_exchange_strong(tail, next,
                                   std::memory_order_release,
                                   std::memory_order_relaxed);
      continue;
    }
    if (Head.compare_exchange_strong(head, next,
                                     std::memory_order_release,
                                     std::memory_order_relaxed)) break;
  } while (true);
  rvalue = next->value;
  hp.reset();
  hp.retire_node(head);
  return true;
}

#endif
