// Simple Hashtbl implem

# ifndef HASHTBL_H_
# define HASHTBL_H_

# include <stdint.h>

# ifndef CAPACITY
# define CAPACITY 67
# endif

struct cell {
  struct cell          *next;
  uint32_t              hkey;
  void                 *ptr;
};

struct htable {
  size_t                size;
  struct cell          *tab[CAPACITY];
};

static inline
uint32_t hash(char *data) {
  uint32_t      h = 0;
  for (char *cur = data; cur != data + sizeof (void*); ++cur) {
    h += *cur;
    h += (h << 10);
    h ^= (h >> 6);
  }
  h += (h << 3);
  h ^= (h >> 11);
  h += (h << 15);
  return h;
}

static inline
void htable_init(struct htable *h, size_t capacity, void *data) {
  h->size = 0;
  h->capacity = capacity;
  h->tab = data;
  for (size_t i = 0; i < capacity; ++i)
    h->tab[i] = NULL;
}

static inline
void htable_add(struct htable *h, struct cell *cell) {
  cell->hkey = hash((char*)&(cell->ptr));
  cell->next = h->tab[cell->hkey % h->capacity];
  h->tab[cell->hkey % h->capacity] = cell;
  h->size += 1;
}

static inline
int htable_find(struct htable *h, void *x) {
  uint32_t              key = hash(&x);
  struct cell          *cur = h->tab[key % h->capacity];
  for (; cur && cur->hkey != key && cur->ptr != x; cur = cur->next) {}
  return cur != NULL;
}

# endif
