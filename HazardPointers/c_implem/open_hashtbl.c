// Simple hash table
// Collision must be solved without using extra-allocation


// Actual implementation use linear probe

# ifndef OPEN_HASHTBL_H_
# define OPEN_HASHTBL_H_

# include <stdint.h>

# ifndef HTBL_CAPACITY
# define HTBL_CAPACITY 131
# endif

struct cell {
  void                 *ptr;
  uint32_t              hkey;
};

struct htable {
  size_t                size;
  struct cell           tab[HTBL_CAPACITY];
};

static inline
void htable_init(struct htable *h) {
  h->size = 0;
  for (struct cell *cur = h->tab; cur != h->tab + HTBL_CAPACITY; ++cur) {
    cur->ptr = NULL;
    cur->hkey = 0;
  }
}

static inline
uint32_t hash(char *data) {
  uint32_t      h = 0;
  for (char *cur = data; cur != data + sizeof (void*); ++cur) {
    h += *cur;
    h += (h << 10);
    h ^= (h >> 6);
  }
  h += (h << 3);
  h ^= (h >> 11);
  h += (h << 15);
  return h;
}

static inline
uint32_t htable_probe(struct htable *h, void *ptr, uint32_t *hk) {
  *hk = hash(&ptr);
  uint32_t              i = hk % HTBL_CAPACITY;
  for (; h->tab[i].hkey != 0 && h->tab[i].ptr != ptr; ++i) {}
  return i;
}

static inline
void htable_add(struct htable *h, void *ptr) {
  if (h->size < HTBL_CAPACITY) {
    uint32_t    hk, i;
    i = htable_probe(h, ptr, &hk);
    if (h->tab[i].ptr == ptr) return;
    h->size += 1;
    h->tab[i].hkey = hk;
    h->tab[i].ptr = ptr;
  }
}

static inline
int htable_mem(struct htable *h, void *ptr) {
  uint32_t    hk, i;
  i = htable_probe(h, ptr, &hk);
  return h->tab[i].ptr == ptr;
}

# endif
