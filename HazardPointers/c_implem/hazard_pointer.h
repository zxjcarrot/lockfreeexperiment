// C implementation of hazard pointers

# ifndef HAZARD_POINTER_H_
# define HAZARD_POINTER_H_

# include "hashtbl.h"

/** Static limits **/

// Number of hp per records
# ifndef K
# define K 2
# endif

// Maximum number of active hprecords
// Limits the number of concurrent workers
# ifndef H
# define H 16
# endif

// Static hprecords number, thus static threshold
# ifndef THRESHOLD
# define THRESHOLD (2 * (H) * (K) + (H) * (K) / 4)
# endif

# define HTBL_CAPACITY ((H) * (K) * 2 + 2 * ((H) % 3) + 1)

# include "open_hashtbl.h"

struct free_list {
  size_t        size;
  void         *data[2 * THRESHOLD];
};

// TODO: capacity settings
// free_list never grow biger than the current threshold
// threshold depends on number of threads
// which can probably be bound
// DONE: using a large threshold

static inline
void free_list_push(struct free_list *l, void *x) {
  l->data[l->size] = x;
  l->size += 1;
}

static inline
void free_list_compact(struct free_list *l) {
  if (l->size > 0) {
    void      **last = l->data + l->size - 1;
    for (; last != l->data && *last != NULL; --last)
      l->size -= 1;
    for (void **cur = l->data; cur != last; ++cur) {
      if (*cur == NULL) {
        *cur = *last;
        l->size -= 1;
        --last;
      }
    }
  }
}

struct hprecord {
  // Hazard pointers are just pointers
  void                 *hp[K];
  volatile int          active;
  struct free_list      waiting;
  // Link to manager
  struct hp_manager    *manager;
};

static inline
int test_and_set(volatile int *flag) {
  // TODO: grab asm code from stos
}

static inline
int try_acquire(struct hprecord *hp) {
  return test_and_set(&(hp->active));
}

static inline
void reset(struct hprecord *hp) {
  for (void **cur = hp->hp; cur != hp->hp + K; ++cur)
    *cur = NULL;
}

static inline
void retire(struct hprecord *hp) {
  reset(hp);
  hp->active = 0; // Use an atomic op ?
}

// maybe more fields later
struct hp_manager {
  struct hprecord       records[H];
};

static inline
void hp_manager_init(struct hp_manager *manger) {
  struct hprecord      *end = manger->records + H;
  for (struct hprecord *cur = manger->records; cur != end; ++cur) {
    cur->active = 0;
    cur->manger = manger;
    cur->waiting.size = 0;
    reset(cur);
  }
}

static inline
struct hprecord* acquire(struct hp_manager* manager) {
  struct hprecord      *end = manager->records + H;
  for (struct hprecord *cur = manager->records; cur != end; ++cur) {
    if (try_acquire(cur)) return cur;
  }
  // Too much threads !!!!
  return NULL;
}

static inline
void scan(struct hprecord *hp) {
  struct hp_manager    *manager = hp->manager;
  struct hprecord      *end = manager->records + H;
  struct htable         set;
  htable_init(&set);
  for (struct hprecord *cur = manager->records; cur != end; ++cur) {
    for (unsigned i = 0; i < K; ++i) {
      // acquire
      void             *ptr = cur->hp[i];
      if (ptr != NULL) htable_add(&set, ptr);
    }
  }

  for () {
  }

}

// Add node to the free list
// trigger scan if needed
static inline
void retire_ptr(struct hprecord *hp, void *ptr) {
  free_list_push(&(hp->waiting), ptr);
  if (hp->waiting.size >= THRESHOLD) {
    scan(hp);
    help_scan(hp);
  }
}

# endif

