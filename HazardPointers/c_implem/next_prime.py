def is_prime(n):
    for i in range(2, 1 + int(n/2)):
        if n % i == 0:
            return False
    return True

def next_prime(n):
    if is_prime(n):
        return n
    else:
        return next_prime(n+1)
