#!/usr/bin/env sh

HAZARD=../tests/bench_nonuniform_hazard_lf
LOCKED=../tests/bench_nonuniform_locked_queue

DATE=`uname -sr | perl -pe "s# #_#g"`-`date +%Y%m%d:%H:%M`
FHAZARD=${DATE}_nonuniform_hazard.csv
FLOCKED=${DATE}_nonuniform_locked.csv

touch ${FHAZARD}
touch ${FLOCKED}

for i in `./value`
do
    echo ${HAZARD} ${i}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
done

for i in `./value`
do
    echo ${LOCKED} ${i}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
done

# Keep for later use
# for i in `./value`
# do
#     echo ${TWOLOC} ${i}
#     ${TWOLOC} ${i} >> ${FTWOLOC}
#     ${TWOLOC} ${i} >> ${FTWOLOC}
#     ${TWOLOC} ${i} >> ${FTWOLOC}
#     ${TWOLOC} ${i} >> ${FTWOLOC}
#     ${TWOLOC} ${i} >> ${FTWOLOC}
# done

# END
