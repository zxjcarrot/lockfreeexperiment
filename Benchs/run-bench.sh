#!/usr/bin/env sh

HAZARD=../tests/bench_hazardQ
LOCKED=../tests/bench_lockedQ
TWOLOC=../tests/bench_two_lock_queue
LFSTAC=../tests/bench_lf_stack
LSTACK=../tests/bench_locked_stack

# DATE=`date +%Y%m%d:%H:%M`
DATE=`uname -sr | perl -pe "s# #_#g"`-`date +%Y%m%d:%H:%M`
FHAZARD=${DATE}_hazard.csv
FLOCKED=${DATE}_locked.csv
FTWOLOC=${DATE}_twoloc.csv
FLFSTAC=${DATE}_lfstac.csv
FLSTACK=${DATE}_lstack.csv

touch ${FHAZARD}
touch ${FLOCKED}
touch ${FTWOLOC}
touch ${FLFSTAC}
touch ${FLSTACK}

for i in `./value`
do
    echo ${HAZARD} ${i}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
    ${HAZARD} ${i} >> ${FHAZARD}
done

for i in `./value`
do
    echo ${LOCKED} ${i}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
    ${LOCKED} ${i} >> ${FLOCKED}
done

for i in `./value`
do
    echo ${TWOLOC} ${i}
    ${TWOLOC} ${i} >> ${FTWOLOC}
    ${TWOLOC} ${i} >> ${FTWOLOC}
    ${TWOLOC} ${i} >> ${FTWOLOC}
    ${TWOLOC} ${i} >> ${FTWOLOC}
    ${TWOLOC} ${i} >> ${FTWOLOC}
done

for i in `./value`
do
    echo ${LFSTAC} ${i}
    ${LFSTAC} ${i} >> ${FLFSTAC}
    ${LFSTAC} ${i} >> ${FLFSTAC}
    ${LFSTAC} ${i} >> ${FLFSTAC}
    ${LFSTAC} ${i} >> ${FLFSTAC}
    ${LFSTAC} ${i} >> ${FLFSTAC}
done

for i in `./value`
do
    echo ${LSTACK} ${i}
    ${LSTACK} ${i} >> ${FLSTACK}
    ${LSTACK} ${i} >> ${FLSTACK}
    ${LSTACK} ${i} >> ${FLSTACK}
    ${LSTACK} ${i} >> ${FLSTACK}
    ${LSTACK} ${i} >> ${FLSTACK}
done

# END
