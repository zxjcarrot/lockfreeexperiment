/*
 * Copyright (c) 2013, Marwan Burelle
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// Simple Non-Concurrent Ring Buffer

// TODO:

#include <functional>

#ifndef LOCKFREEEXPERIMENT_RINGBUFFER_NO_THREAD_RING_BUFFER_HH_
#define LOCKFREEEXPERIMENT_RINGBUFFER_NO_THREAD_RING_BUFFER_HH_

template<typename T>
class bounded_queue {
public:
  bounded_queue(unsigned capacity_)
  : head(0), size(0), capacity(capacity_), store(new T[capacity_]) {}

  bool is_empty() { return size == 0; }
  bool is_full() { return size == capacity; }
  unsigned getsize() { return size; }
  unsigned gethead() { return head; }

  bool push(const T& x) {
    if (size < capacity) {
      store[(head + size) % capacity] = x;
      size += 1;
      return true;
    }
    return false;
  }

  bool pop(T& x) {
    if (size > 0) {
      x = store[head];
      head = (head + 1) % capacity;
      size -= 1;
      return true;
    }
    return false;
  }

  bool top(T& x) {
    if (size > 0) {
      x = store[head];
      return true;
    }
    return false;
  }

  void iter(std::function<void(T&)> f) {
    for (unsigned i = 0; i < size; ++i)
      f(store[ (i + head) % capacity ]);
  }

private:
  unsigned      head, size, capacity;
  T            *store;
};

#endif
